package cat.almata.a006;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

//	Aquesta sera la meva finestra per a l'aplicacio

public class Finestra extends JFrame implements Formulari{
	
	private static final long serialVersionUID = 1L;
	
	//	Components del formulari
	private JLabel lblNom;
	private JTextField txtNom;

	public Finestra() {
		inicialitzacions();
	}

	@Override
	public void inicialitzacions() {
		setBounds(500, 200, 400, 200);
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Formulari entrada nom.");
		
	}

	@Override
	public void crearComponents() {
		lblNom=new JLabel("Nom:	");
		txtNom=new JTextField(20);
		
	}

	@Override
	public void afegirComponents() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void posicionarComponents() {
		// TODO Auto-generated method stub
		
	}
	
}

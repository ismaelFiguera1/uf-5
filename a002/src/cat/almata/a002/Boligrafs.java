package cat.almata.a002;

public class Boligrafs {
	private String color;

	public Boligrafs(String color) {
		super();
		this.color = color;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "Boligrafs [color=" + color + "]";
	}
	
	
}

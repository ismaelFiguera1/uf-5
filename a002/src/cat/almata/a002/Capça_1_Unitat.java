package cat.almata.a002;

public class Capça_1_Unitat<T extends Galeta> {
	T element;
	private String nom;

	public Capça_1_Unitat(String nom) {
		super();
		setNom(nom);
	}

	public T getElement() {
		return element;
	}

	public void setElement(T element) {
		this.element = element;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	
	
	
	
	
}

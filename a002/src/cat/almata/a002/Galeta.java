package cat.almata.a002;

public class Galeta {
	private String gust;
	
	public Galeta(String gust) throws Exception {
		setGust(gust);
	}

	public Galeta() {
		super();
	}

	public String getGust() {
		return gust;
	}

	public void setGust(String gust) throws Exception {
		if(gust.length()<5) {
			this.gust = gust;
		}else {
			throw new Exception("El nom del gust massa llarg");
		}
		
	}
	
	
	
}

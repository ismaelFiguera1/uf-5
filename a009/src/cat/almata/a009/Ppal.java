package cat.almata.a009;

import java.awt.BorderLayout;

import cat.almata.a009.presentacio.Aplicacio;
import cat.almata.a009.presentacio.ControladorDePresentacio;
import cat.almata.a009.presentacio.FormulariPerDefecte;
import cat.almata.a009.presentacio.FormulariAltaUsuari;

public class Ppal {
	public static void main(String[] args) {
//		Crear finestra
			ControladorDePresentacio.crearAplicacio();
			ControladorDePresentacio.canviFormulari(new FormulariPerDefecte());
			

	}
}

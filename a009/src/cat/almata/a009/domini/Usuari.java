package cat.almata.a009.domini;

import java.util.Objects;

public class Usuari {
	private String dni;
	private String nom;
	private String cognom;
	private int edat;
	private String dataNaixement;
	private String sexe;
	private String observacions;
	
	
	public Usuari() {
		super();
	}
	
	public Usuari(String dni) {
		super();
		this.dni = dni;
	}
	
	public Usuari(String dni, String nom, String cognom, int edat, String dataNaixement, String sexe,
			String observacions) {
		super();
		this.dni = dni;
		this.nom = nom;
		this.cognom = cognom;
		this.edat = edat;
		this.dataNaixement = dataNaixement;
		this.sexe = sexe;
		this.observacions = observacions;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCognom() {
		return cognom;
	}

	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public String getDataNaixement() {
		return dataNaixement;
	}

	public void setDataNaixement(String dataNaixement) {
		this.dataNaixement = dataNaixement;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public String getObservacions() {
		return observacions;
	}

	public void setObservacions(String observacions) {
		this.observacions = observacions;
	}

	@Override
	public String toString() {
		return "Usuari [dni=" + dni + ", nom=" + nom + ", cognom=" + cognom + ", edat=" + edat + ", dataNaixement="
				+ dataNaixement + ", sexe=" + sexe + ", observacions=" + observacions + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(dni);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuari other = (Usuari) obj;
		return Objects.equals(dni, other.dni);
	}
	
	
	
	
	
	
}

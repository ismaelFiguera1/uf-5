package cat.almata.a009.bbdd;

import java.util.Map;
import java.util.TreeMap;

import cat.almata.a009.domini.Usuari;

public class HelperBBDDMem {

	private static Map<String, Usuari> taulaUsuaris = new TreeMap<String, Usuari>(); 
	
	public static void addUser(Usuari user) {
		taulaUsuaris.put(user.getDni(), user);
	}
	public static Usuari getUser(String keyDNI) {
		return taulaUsuaris.get(keyDNI);
	}
	public static void remove(String keyDNI) {
		taulaUsuaris.remove(keyDNI);
		
	}
	
	public static Map<String, Usuari> getAllUsers(){
		return taulaUsuaris;
	}
	
}

package cat.almata.a009.presentacio;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.github.lgooddatepicker.components.DatePicker;

import cat.almata.a009.utils.Fonts;
import cat.almata.a009.utils.Util;



public class FormulariAltaUsuari extends JInternalFrame implements Formulari {
	
	private static final long serialVersionUID = 1L;
	
	private GridBagLayout layout;
	private JLabel lblNom;
	private JTextField txtNom;
	private JLabel lblDni;
	private JTextField txtDni;
	private JLabel lblCognom;
	private JTextField txtCognom;
	private JLabel lblEdat;
	private JTextField txtEdat;
	private JLabel lblNaixement;
	private DatePicker dtpNaixement;
	private JLabel lblSexe;
	private JLabel lblObservacions;
	private JRadioButton rbdHome;
	private JRadioButton rbdDona;
	private JTextArea txtObservacions;
	private JButton btnAcceptar;
	private JButton btnCancelar;
	private ButtonGroup grupBotons;
	private JPanel panellBotons;
	private Controlador controlador;
	
	
	
	public FormulariAltaUsuari() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
	}

	@Override
	public void inicialitzacions() {
		setTitle("Formulari entrada dades de l'Usuari");
		getContentPane().setLayout(layout=new GridBagLayout());
		Util.treureBarraTitolInteralFrame(this);
		controlador=new Controlador();
		this.setVisible(true);
	}

	@Override
	public void crearComponents() {
		lblCognom=new JLabel("Cognoms:");
		lblCognom.setFont(Fonts.fontLabel());
		txtCognom=new JTextField(20);
		lblEdat=new JLabel("Edat:");
		txtEdat=new JTextField(20);
		lblNaixement=new JLabel("Data de naixement:");
		dtpNaixement=new DatePicker();
		lblNom=new JLabel("Nom:");
		txtNom=new JTextField(20);
		lblNom.setFont(Fonts.fontLabel());
		txtNom.setFont(Fonts.fontTextField());
		txtNom.setToolTipText("Cal introduir el nom.");
		lblSexe=new JLabel("Sexe:");
		lblObservacions=new JLabel("Observacions:");
		rbdDona=new JRadioButton("Dona");
		rbdHome=new JRadioButton("Home");
		grupBotons=new ButtonGroup();
		grupBotons.add(rbdDona);
		grupBotons.add(rbdHome);
		txtObservacions=new JTextArea();
		btnAcceptar=new JButton("Acceptar");
		btnAcceptar.addActionListener(controlador);
		btnAcceptar.setActionCommand("acceptar");
		btnCancelar=new JButton("Cancelar");
		btnCancelar.addActionListener(controlador);
		btnCancelar.setActionCommand("cancelar");
		panellBotons=new JPanel();
		panellBotons.setLayout(new GridLayout(1, 2, 5, 0));	
		panellBotons.add(btnAcceptar);
		panellBotons.add(btnCancelar);
		lblDni=new JLabel("Dni:");
		lblDni.setFont(Fonts.fontLabel());
		txtDni= new JTextField(20);
		txtDni.setFont(Fonts.fontTextField());
	}

	@Override
	public void afegirComponents() {
		getContentPane().add(lblCognom);
		getContentPane().add(txtCognom);
		getContentPane().add(lblEdat);
		getContentPane().add(txtEdat);
		getContentPane().add(lblNaixement);
		getContentPane().add(dtpNaixement);
		getContentPane().add(lblNom);
		getContentPane().add(txtNom);
		getContentPane().add(lblSexe);
		getContentPane().add(lblObservacions);
		getContentPane().add(rbdDona);
		getContentPane().add(rbdHome);
		getContentPane().add(txtObservacions);
		getContentPane().add(panellBotons);
		getContentPane().add(lblDni);
		getContentPane().add(txtDni);

	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();
		
		//lblDni
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblDni, gbc);
		
		//txtDni
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtDni, gbc);
		
		
		//lblNom
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=1;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblNom, gbc);
		
		//txtNom
		gbc.gridx=1;		//	primera columna
		gbc.gridy=1;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtNom, gbc);
		
		
		//lblCognom
		gbc.gridx=0;		//	primera columna
		gbc.gridy=2;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblCognom, gbc);
		
		//txtCognom
		gbc.gridx=1;		//	primera columna
		gbc.gridy=2;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtCognom, gbc);
		
		
		//lblEdat
		gbc.gridx=0;		//	primera columna
		gbc.gridy=3;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblEdat, gbc);
		
		//txtEdat
		gbc.gridx=1;		//	primera columna
		gbc.gridy=3;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtEdat, gbc);
		
		
		//lblNaixement
		gbc.gridx=0;		//	primera columna
		gbc.gridy=4;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblNaixement, gbc);
		
		//txtNaixement
		gbc.gridx=1;		//	primera columna
		gbc.gridy=4;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(dtpNaixement, gbc);
		
		
		//lblSexe
		gbc.gridx=0;		//	primera columna
		gbc.gridy=5;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblSexe, gbc);
		
		//rbdHome
		gbc.gridx=1;		//	primera columna
		gbc.gridy=5;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(rbdHome, gbc);
		
		//rbdDona
		gbc.gridx=1;		//	primera columna
		gbc.gridy=6;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(rbdDona, gbc);
		
		
		//lblObservacions
		gbc.gridx=0;		//	primera columna
		gbc.gridy=7;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblObservacions, gbc);
		
		//txtObservacions
		gbc.gridx=0;		//	primera columna
		gbc.gridy=8;		//	primera fila
		gbc.gridheight=3;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=1;		//	factor de creixement
		gbc.weighty=1;		//	factor de creixement
		//gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.BOTH;
		layout.setConstraints(txtObservacions, gbc);
		
		
		//panellBotons
		gbc.gridx=1;		//	primera columna
		gbc.gridy=11;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.EAST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(panellBotons, gbc);
		


	}
	private class Controlador implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if(obj instanceof JButton) {
				JButton boto = (JButton) obj;
				if(boto.getActionCommand().equals("acceptar")) {
					String sexe=null;
					

					if(rbdDona.isSelected()) {
	
						sexe="Dona";
					}
					else if (rbdHome.isSelected()) {

						sexe="Home";
					}
					else System.out.println("Sexe no seleccionat");

					
					
					
					
					
					ControladorDePresentacio.addUser(txtDni.getText(),
							txtNom.getText(),
							txtCognom.getText(),
							Integer.valueOf(txtEdat.getText()),
							dtpNaixement.getText(),
							sexe,
							txtObservacions.getText());

					System.out.println("L'alta del usuari s'ha fet correctament");
					
					
					esborrarFormulari();
				}else if(boto.getActionCommand().equals("cancelar")) {
					System.out.println("Cancelar");
					esborrarFormulari();
				}
				
			}
			
		}
		private void esborrarFormulari() {
			txtDni.setText("");
			txtNom.setText("");
			txtCognom.setText("");
			txtEdat.setText("");
			dtpNaixement.setText("");
			grupBotons.clearSelection();
			txtObservacions.setText("");
			txtDni.grabFocus();
		}
	}



}

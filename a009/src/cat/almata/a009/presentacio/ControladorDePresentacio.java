package cat.almata.a009.presentacio;

import java.awt.BorderLayout;
import java.util.Map;

import javax.swing.JInternalFrame;

import cat.almata.a009.bbdd.HelperBBDDMem;
import cat.almata.a009.domini.Usuari;

public class ControladorDePresentacio {

	private static Aplicacio aplicacio=null;
	private static JInternalFrame actual=null;
	
	
	public static void crearAplicacio() {
		if(aplicacio==null) {
			aplicacio=new Aplicacio();
		}
	}
	
	public static void canviFormulari(JInternalFrame nou) {
		if(actual!=null) 
			actual.dispose();
			aplicacio.getContentPane().add(nou, BorderLayout.CENTER);
			aplicacio.setVisible(true);
			actual=nou;
		
	}
	
	// metodes de negoci
	public static void addUser(String dni, String nom,
								String cognoms, int edat,
								String dataNaixement,
								String sexe,
								String observacions) {
		HelperBBDDMem.addUser(new Usuari(dni, nom, cognoms, edat, dataNaixement, sexe, observacions));
	}
	
	public static Usuari getUser(String keyDNI) {
		return HelperBBDDMem.getUser(keyDNI);
	}
	
	public static void removeUser(String keyDNI) {
		HelperBBDDMem.remove(keyDNI);
	}
	
	public static Map<String, Usuari> getAllUsers(){
		return HelperBBDDMem.getAllUsers();
	}
	
}

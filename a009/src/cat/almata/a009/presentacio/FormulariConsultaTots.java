package cat.almata.a009.presentacio;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;



import cat.almata.a009.domini.Usuari;




public class FormulariConsultaTots extends JInternalFrame implements Formulari {
	private static final long serialVersionUID = 1L;
	
	private GridBagLayout layout;
	private Controlador controlador;
	
	
	private JButton btnSortir;
	private JTable taula;
	private JScrollPane jspTaula;
	
	private DefaultTableModel model;
	
	public FormulariConsultaTots() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
	}
	
	@Override
	public void inicialitzacions() {
		setTitle("Formulari consulta tots els usuaris");
		getContentPane().setLayout(layout=new GridBagLayout());
		controlador=new Controlador();
		this.setVisible(true);
	}

	@Override
	public void crearComponents() {
		
		btnSortir=new JButton("Sortir");
		btnSortir.addActionListener(controlador);
		btnSortir.setActionCommand("sortir");
		model = new DefaultTableModel();
		model.addColumn("DNI");
		model.addColumn("Nom");
		model.addColumn("Cognoms");
		model.addColumn("Edat");
		
		taula = new JTable(model);
		
		jspTaula=new JScrollPane(taula);
		
		omplirTaula();
	}

	private void omplirTaula() {
		Map<String, Usuari> usuaris = ControladorDePresentacio.getAllUsers();
		for(Entry<String, Usuari> objecteMapa: usuaris.entrySet()) {
			Usuari usuari = objecteMapa.getValue();
			Object[] objFila=new Object[4];
			objFila[0]= usuari.getDni();
			objFila[1]= usuari.getNom();
			objFila[2]= usuari.getCognom();
			objFila[3]= usuari.getEdat();
			model.addRow(objFila);
		}
		btnSortir=new JButton("Sortir");
		btnSortir.addActionListener(controlador);
		btnSortir.setActionCommand("sortir");
	}

	@Override
	public void afegirComponents() {
		add(btnSortir);
		add(jspTaula);

	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();
		//txtDni
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=4;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=1;		//	factor de creixement
		gbc.weighty=1;		//	factor de creixement
	//	gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.BOTH;
		layout.setConstraints(jspTaula, gbc);
		
		
		
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=5;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
	//	gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.NONE;
		layout.setConstraints(btnSortir, gbc);

	}
	
	private class Controlador implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if (obj instanceof JButton) {
				JButton boto = (JButton) obj;
				if(boto.getActionCommand().equals("sortir")) {
					System.out.println("Sortir");
					ControladorDePresentacio.canviFormulari(new FormulariPerDefecte());
				}
			} 
		}
		
	}

}

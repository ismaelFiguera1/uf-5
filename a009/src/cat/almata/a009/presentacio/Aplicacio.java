package cat.almata.a009.presentacio;



import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import cat.almata.a009.utils.Util;

public class Aplicacio extends JFrame{
	private static final long serialVersionUID = 1L;
	public static final int With = 400;
	public static final int height = 500;
	
	private JMenu menuGestio;
	private JMenuItem opcioAlta;
	private JMenuItem opcioConsulta;
	private JMenuItem opcioConsultaTot;
	private JMenuItem opcioModificacio;
	private JMenuItem opcioEsborrar;
	
	private JMenu menuIdioma;
	private JMenuItem idiomaCatala;
	private JMenuItem idiomaCastella;
	private Controlador controlador;
	
	private JMenu menuTraduir;
	private JCheckBoxMenuItem idiomaIngles;
	private JCheckBoxMenuItem idiomaXines;
	

	public Aplicacio() {
		inicialitzacio();
	}

	private void inicialitzacio() {
		setBounds(0, 0, With, height);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Aplicacio");
		Util.centrarFinestra(this);
		Container c = this.getContentPane();
		((JComponent)c).setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		this.setIconImage(new ImageIcon("imatges/images.jpeg").getImage());
		controlador = new Controlador();
		this.setJMenuBar(crearMenu());
	}
	
	private JMenuBar crearMenu() {
		JMenuBar barraMenu = new JMenuBar();
		//	opcio de menu
		menuGestio=new JMenu("Gestio");
		//	afegir opcio a la barra menu

		opcioAlta=new JMenuItem("Alta");
		opcioAlta.addActionListener(controlador);
		opcioAlta.setMnemonic(KeyEvent.VK_A);
		opcioAlta.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,ActionEvent.ALT_MASK));
		opcioAlta.setActionCommand("alta");

		
		opcioConsulta=new JMenuItem("Consulta");
		opcioConsulta.addActionListener(controlador);
		opcioConsulta.setMnemonic(KeyEvent.VK_C);
		opcioConsulta.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.ALT_MASK));
		opcioConsulta.setActionCommand("consulta");
		
		
		opcioModificacio=new JMenuItem("Modificacio");
		opcioModificacio.addActionListener(controlador);
		opcioModificacio.setMnemonic(KeyEvent.VK_M);
		opcioModificacio.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M,ActionEvent.ALT_MASK));
		opcioModificacio.setActionCommand("modificacio");
		
		
		opcioEsborrar=new JMenuItem("Esborrar");
		opcioEsborrar.addActionListener(controlador);
		opcioEsborrar.setMnemonic(KeyEvent.VK_E);
		opcioEsborrar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,ActionEvent.ALT_MASK));
		opcioEsborrar.setActionCommand("esborrar");
		
		
		opcioConsultaTot=new JMenuItem("Consulta tots");
		opcioConsultaTot.addActionListener(controlador);
		opcioConsultaTot.setMnemonic(KeyEvent.VK_C);
		opcioConsultaTot.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.ALT_MASK));
		opcioConsultaTot.setActionCommand("tots");
		
		menuGestio.add(opcioAlta);
		menuGestio.addSeparator();
		menuGestio.add(opcioConsulta);
		menuGestio.addSeparator();
		menuGestio.add(opcioModificacio);
		menuGestio.addSeparator();
		menuGestio.add(opcioEsborrar);
		menuGestio.addSeparator();
		menuGestio.add(opcioConsultaTot);
		barraMenu.add(menuGestio);
		
		
		menuIdioma=new JMenu("Idiomes");

		
		idiomaCatala=new JMenuItem("Catala");
		idiomaCatala.addActionListener(controlador);
		idiomaCatala.setMnemonic(KeyEvent.VK_C);
		idiomaCatala.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.ALT_MASK));
		idiomaCatala.setActionCommand("catala");

		
		idiomaCastella=new JMenuItem("Castella");
		idiomaCastella.addActionListener(controlador);
		idiomaCastella.setMnemonic(KeyEvent.VK_S);
		idiomaCastella.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,ActionEvent.ALT_MASK));
		idiomaCastella.setActionCommand("castella");
		
		menuIdioma.add(idiomaCatala);
		menuIdioma.addSeparator();
		menuIdioma.add(idiomaCastella);
		barraMenu.add(menuIdioma);
		
		
		menuTraduir=new JMenu("Traduccio");
		barraMenu.add(menuTraduir);
		
		idiomaIngles=new JCheckBoxMenuItem("Ingles");
		idiomaIngles.addActionListener(controlador);
		idiomaIngles.setMnemonic(KeyEvent.VK_I);
		idiomaIngles.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I,ActionEvent.ALT_MASK));
		idiomaIngles.setActionCommand("ingles");

		
		idiomaXines=new JCheckBoxMenuItem("Xines");
		idiomaXines.addActionListener(controlador);
		idiomaXines.setMnemonic(KeyEvent.VK_X);
		idiomaXines.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X,ActionEvent.ALT_MASK));
		idiomaXines.setActionCommand("xines");
		
		ButtonGroup bg = new ButtonGroup();
		bg.add(idiomaXines);
		bg.add(idiomaIngles);
		
		menuTraduir.add(idiomaIngles);
		menuTraduir.addSeparator();
		menuTraduir.add(idiomaXines);
		barraMenu.add(menuTraduir);
		
		
		return barraMenu;
		
	}

	
	
	private class Controlador implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if(obj instanceof JMenuItem && !(obj instanceof JCheckBoxMenuItem)) {
				JMenuItem jmi = (JMenuItem) obj;
				if(jmi.getActionCommand().equals("alta")) {
					System.out.println("alta");
					ControladorDePresentacio.canviFormulari(new FormulariAltaUsuari());
				}else if(jmi.getActionCommand().equals("consulta")){
					System.out.println("consulta");
					ControladorDePresentacio.canviFormulari(new FormulariConsultaUsuari());
				}else if(jmi.getActionCommand().equals("modificacio")){
					System.out.println("modificacio");
					ControladorDePresentacio.canviFormulari(new FormulariModificacioUsuari());
				}else if(jmi.getActionCommand().equals("esborrar")){
					System.out.println("esborrar");
					ControladorDePresentacio.canviFormulari(new FormulariEsborrarUsuari());
				}else if(jmi.getActionCommand().equals("tots")){
					System.out.println("Consulta Tots");
					ControladorDePresentacio.canviFormulari(new FormulariConsultaTots());
				}else if(jmi.getActionCommand().equals("catala")) {
					System.out.println("catala");
				}else if(jmi.getActionCommand().equals("castella")) {
					System.out.println("castella");
				}
			}else if (obj instanceof JCheckBoxMenuItem) {
				JCheckBoxMenuItem jck = (JCheckBoxMenuItem) obj;
				if(jck.getActionCommand().equals("ingles")) {
					System.out.println("ingles");
				}else if(jck.getActionCommand().equals("xines")){
					System.out.println("xines");
				}
			}
			
		}
		
	}
}

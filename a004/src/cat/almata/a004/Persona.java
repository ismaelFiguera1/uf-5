package cat.almata.a004;


import java.util.Objects;

public class Persona {
	private String nom;
	private String dni;
	
	
	
	public Persona(String nom, String dni) {
		super();
		setDni(dni);
		setNom(nom);
	}
	
	
	
	
	
	
	public Persona(String dni) {
		super();
		this.dni = dni;
	}






	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	@Override
	public String toString() {
		return "Persona [nom=" + nom + ", dni=" + dni + "]";
	}
	@Override
	public int hashCode() {
		return Objects.hash(dni);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		return Objects.equals(dni, other.dni);
	}
	
	
	
	

}

package cat.almata.a004;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Ppal {
	public static void main(String[] args) {
		
		/*
		Al List accedim per la posicio (index).
		Al Set busca pe columna clau.
		La columna clau son els atributs que es comparen al equals de la classe Persona.
		*/
		
		Set<Persona> persones = new HashSet<Persona>();
		persones.add(new Persona("Alex", "a001"));
		persones.add(new Persona("Julia", "a002"));
		
		//persones.
		Set<String> cadenes = new HashSet<String>();
		
		cadenes.add("hola");
		cadenes.add("que");
		cadenes.add("hola");
		cadenes.add("tal");
	/*	
		for(String a : cadenes) {
			System.out.println(a);
		}
		*/
		
		
		
		Set<String> cadenes2 = new TreeSet<String>();
		cadenes2.add("figuera");
		cadenes2.add("agelet");
		cadenes2.add("zohir");
		cadenes2.add("benet");
		/*
		for(String i : cadenes2) {
			System.out.println(i);
		}
		*/
		
		
		
		
		/*	Creem un array i el passem a list, passem el list a set (perque list i set descendeixen de collection)
		i imprimim el set.*/
		String[] llista = {"Hola","noi","tal","estas","noi"};
		for(String a:llista) {
			System.out.println(a);
		}
		//	Elimina les cadenes duplicades i despres ordena-ho
		List<String> llista2 = Arrays.asList(llista);
		Set<String> cadenes3 = new TreeSet<String>(llista2);
		System.out.println("Llista sense repeticions i ordenada");
		for(String i:cadenes3) {
			System.out.println(i);
		}
		
	}
}

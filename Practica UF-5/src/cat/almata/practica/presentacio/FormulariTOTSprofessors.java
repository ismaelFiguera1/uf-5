package cat.almata.practica.presentacio;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;



import cat.almata.practica.domini.Professor;





public class FormulariTOTSprofessors extends JInternalFrame implements Formulari {
	private static final long serialVersionUID = 1L;
	
	private GridBagLayout layout;
	private Controlador controlador;
	
	private JButton btnSortir;
	private JTable taula;
	private JScrollPane jspTaula;
	
	private DefaultTableModel model;
	
	public FormulariTOTSprofessors() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
	}
	
	
	@Override
	public void inicialitzacions() {
		// TODO Auto-generated method stub
		setTitle("Formulari consulta tots els professors");
		getContentPane().setLayout(layout=new GridBagLayout());
		controlador=new Controlador();
		this.setVisible(true);
	}

	@Override
	public void crearComponents() {
		// TODO Auto-generated method stub
		btnSortir=new JButton("Sortir");
		btnSortir.addActionListener(controlador);
		btnSortir.setActionCommand("sortir");
		
		
		
		model = new DefaultTableModel();
		model.addColumn("DNI");
		model.addColumn("Nom_Usuari");
		model.addColumn("Nom");
		model.addColumn("Cognoms");
		
		
		
		taula = new JTable(model);
		
		
		
		jspTaula=new JScrollPane(taula);
		
		omplirTaula();
	}
	
	private void omplirTaula() {
		Map<String, Professor> professors = Controlador_de_Presentacio.getAllProfessors();
		for(Entry<String, Professor> objecteMapa: professors.entrySet()) {
			Professor profe = objecteMapa.getValue();
			Object[] objFila=new Object[4];
			objFila[0]=profe.getDni();
			objFila[1]=profe.getNomUsuari();
			objFila[2]=profe.getNom();
			objFila[3]=profe.getCognoms();
			model.addRow(objFila);
		}
	}
	
	
	
	
	
	
	

	@Override
	public void afegirComponents() {
		// TODO Auto-generated method stub
		add(btnSortir);
		add(jspTaula);
	}

	@Override
	public void posicionarComponents() {
		// TODO Auto-generated method stub
		GridBagConstraints gbc = new GridBagConstraints();
		
		//jspTaula
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=5;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=1;		//	factor de creixement
		gbc.weighty=1;		//	factor de creixement
	//	gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.BOTH;
		layout.setConstraints(jspTaula, gbc);
		
		
		
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=6;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
	//	gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.NONE;
		layout.setConstraints(btnSortir, gbc);
	}
	private class Controlador implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Object obj = e.getSource();
			if (obj instanceof JButton) {
				JButton boto = (JButton) obj;
				if(boto.getActionCommand().equals("sortir")) {
					Controlador_de_Presentacio.canviFormulari(new FormulariPerDefecte());
				}
			} 
		}
		
	}
}

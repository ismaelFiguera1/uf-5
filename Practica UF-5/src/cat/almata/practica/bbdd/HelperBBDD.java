package cat.almata.practica.bbdd;

import java.util.Map;
import java.util.TreeMap;


import cat.almata.practica.domini.Alumne;
import cat.almata.practica.domini.Professor;

public class HelperBBDD {
	private static Map<String, Alumne> taulaAlumnes = new TreeMap<String, Alumne>();
	private static Map<String, Professor> taulaProfessors = new TreeMap<String, Professor>();
	
	
	public static void addAlumne(Alumne alumne) {
		taulaAlumnes.put(alumne.getDni(), alumne);
	}
	public static Alumne getAlumne(String keyDni) {
		return taulaAlumnes.get(keyDni);
	}
	public static Map<String, Alumne> getAllAlumnes(){
		return taulaAlumnes;
	}
	
	
	
	
	
	
	public static void addProfessor(Professor professor) {
		taulaProfessors.put(professor.getDni(), professor);
	}
	public static Professor getProfessor(String keyDni) {
		return taulaProfessors.get(keyDni);
	}
	public static Map<String, Professor> getAllProfessors(){
		return taulaProfessors;
	}
	public static void removeAlumne(String keyDNI) {
		taulaAlumnes.remove(keyDNI);
	}
	public static void removeProfessor(String keyDNI) {
		taulaProfessors.remove(keyDNI);
	}
}

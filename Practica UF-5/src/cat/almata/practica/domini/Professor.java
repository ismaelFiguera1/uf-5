package cat.almata.practica.domini;


import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

public class Professor {
	private String dni;
	private String nomUsuari;
	private String nom;
	private String cognoms;
	private String dataNaixement;
	private String poblacio;
	private int codiPostal;
	private boolean oposicions;
	
	
	Map<String,Alumne> alumnes = new TreeMap<String,Alumne>();
	
	
	public Professor(String dni, String nomUsuari, String nom, String cognoms, String dataNaixement, String poblacio,
			int codiPostal, boolean oposicions) {
		super();
		this.dni = dni;
		this.nomUsuari = nomUsuari;
		this.nom = nom;
		this.cognoms = cognoms;
		this.dataNaixement = dataNaixement;
		this.poblacio = poblacio;
		this.codiPostal = codiPostal;
		this.oposicions = oposicions;
	}


	public String getDni() {
		return dni;
	}


	public void setDni(String dni) {
		this.dni = dni;
	}


	public String getNomUsuari() {
		return nomUsuari;
	}


	public void setNomUsuari(String nomUsuari) {
		this.nomUsuari = nomUsuari;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getCognoms() {
		return cognoms;
	}


	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}


	public String getDataNaixement() {
		return dataNaixement;
	}


	public void setDataNaixement(String dataNaixement) {
		this.dataNaixement = dataNaixement;
	}


	public String getPoblacio() {
		return poblacio;
	}


	public void setPoblacio(String poblacio) {
		this.poblacio = poblacio;
	}


	public int getCodiPostal() {
		return codiPostal;
	}


	public void setCodiPostal(int codiPostal) {
		this.codiPostal = codiPostal;
	}


	public boolean isOposicions() {
		return oposicions;
	}


	public void setOposicions(boolean oposicions) {
		this.oposicions = oposicions;
	}


	public Map<String,Alumne> getAlumnes() {
		return alumnes;
	}


	public void addAlumne(Alumne a) {
		alumnes.put(a.getDni(),a);
	}
/*	
	public void removeBotella(Alumne a) {
		alumnes.remove(a);
	}
*/

	@Override
	public String toString() {
		return "Professor [dni=" + dni + ", nomUsuari=" + nomUsuari + ", nom=" + nom + ", cognoms=" + cognoms
				+ ", dataNaixement=" + dataNaixement + ", poblacio=" + poblacio + ", codiPostal=" + codiPostal
				+ ", oposicions=" + oposicions + ", alumnes=" + alumnes + "]";
	}


	@Override
	public int hashCode() {
		return Objects.hash(dni);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Professor other = (Professor) obj;
		return Objects.equals(dni, other.dni);
	}
	
	
	
}

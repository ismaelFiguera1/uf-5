package cat.almata.a001;

import java.util.ArrayList;

public class Persona {
	private String nom;
	private String dni;
	
	
	//	metode de negoci
	
	public void addNota(Integer a) {
		notes.add(a);
	}
	
	public ArrayList<Integer> retornNotes(){
		return notes;
	}
	
	public double mitjanaNotes() throws Exception {
		double suma=0;
		double mitjana;
		if(notes.size()!=0) {
			for(Integer nota : notes) {
				suma = suma + nota;
			}
			mitjana = suma / notes.size(); 
		}else {
			throw new Exception("No hi ha notes,no puc fer la mitjana.");
		}
		return mitjana;
	}
	
	public Persona(String nom, String dni) {
		super();
		setNom(nom);
		setDni(dni);
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getDni() {
		return dni;
	}



	public void setDni(String dni) {
		this.dni = dni;
	}



	private ArrayList<Integer> notes = new ArrayList<Integer>();
	
}

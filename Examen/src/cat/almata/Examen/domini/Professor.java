package cat.almata.Examen.domini;

import java.util.Objects;

public class Professor {
	private String imatge;
	private String usuari;
	private String nom;
	private String cognom;
	private int edat;
	private String dataNaixement;
	private boolean carnet;
	
	
	
	public Professor(String imatge, String usuari, String nom, String cognom, int edat, String dataNaixement,
			boolean carnet) {
		super();
		this.imatge = imatge;
		this.usuari = usuari;
		this.nom = nom;
		this.cognom = cognom;
		this.edat = edat;
		this.dataNaixement = dataNaixement;
		this.carnet = carnet;
	}
	
	public Professor(String usuari) {
		super();
		this.usuari = usuari;
	}
	
	public Professor() {
		super();
	}

	public String getImatge() {
		return imatge;
	}

	public void setImatge(String imatge) {
		this.imatge = imatge;
	}

	public String getUsuari() {
		return usuari;
	}

	public void setUsuari(String usuari) {
		this.usuari = usuari;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCognom() {
		return cognom;
	}

	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public String getDataNaixement() {
		return dataNaixement;
	}

	public void setDataNaixement(String dataNaixement) {
		this.dataNaixement = dataNaixement;
	}



	

	public boolean isCarnet() {
		return carnet;
	}

	public void setCarnet(boolean carnet) {
		this.carnet = carnet;
	}
	
	
	

	@Override
	public String toString() {
		return "Professor [imatge=" + imatge + ", usuari=" + usuari + ", nom=" + nom + ", cognom=" + cognom + ", edat="
				+ edat + ", dataNaixement=" + dataNaixement + ", carnet=" + carnet + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(usuari);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Professor other = (Professor) obj;
		return Objects.equals(usuari, other.usuari);
	} 
	
	
}

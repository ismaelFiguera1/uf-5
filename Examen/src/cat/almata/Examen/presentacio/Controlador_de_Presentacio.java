package cat.almata.Examen.presentacio;

import java.awt.BorderLayout;
import java.util.Map;

import javax.swing.JInternalFrame;

import cat.almata.Examen.bbdd.HelperBBDD;
import cat.almata.Examen.domini.Professor;



public class Controlador_de_Presentacio {
	private static Aplicacio aplicacio=null;
	private static JInternalFrame actual=null;
	
	public static void crearAplicacio() {
		if(aplicacio==null) {
			aplicacio=new Aplicacio();
		}
	}
	
	public static void canviFormulari(JInternalFrame nou) {
		if(actual!=null) 
			actual.dispose();
			aplicacio.getContentPane().add(nou, BorderLayout.CENTER);
			aplicacio.setVisible(true);
			actual=nou;
		
	}
	
	public static void addProfessor(String imatge,
			String usuari,
			String nom,
			String cognom,
			int edat,
			String dataNaixement,
			boolean carnet
			) {
		HelperBBDD.addProfessor(new Professor(imatge,usuari,nom,cognom,edat,dataNaixement,carnet));
	}
	
	
	public static Map<String, Professor> getAllUsers(){
		return HelperBBDD.getAllProfessors();
	}
}

package cat.almata.Examen.presentacio;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import cat.almata.Examen.utils.Util;

public class Aplicacio extends JFrame {
	private static final long serialVersionUID = 1L;
	public static final int With = 400;
	public static final int Height=500;
	
	private Controlador controlador;
	private JMenu menuGestio;
	private JMenuItem opcioAltaProfessors;
	private JMenuItem opcioAssignacions;
	
	public Aplicacio() {
		inicialitzacio();
	}
	
	private void inicialitzacio() {
		setBounds(0, 0, 500, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Aplicacio de entrada i sortida de professors");
		Util.centrarFinestra(this);
		/*
		 * Les 2 linees de baix no se que fan.
		 */
		Container c = this.getContentPane();
		((JComponent)c).setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		this.setIconImage(new ImageIcon("imatges/images.jpeg").getImage());
		controlador = new Controlador();
		this.setJMenuBar(crearMenu());
	}
	
	private JMenuBar crearMenu() {
		JMenuBar barraMenu = new JMenuBar();
		menuGestio=new JMenu("Gestio");
		
		opcioAltaProfessors=new JMenuItem("Alta Professor");
		opcioAltaProfessors.setMnemonic(KeyEvent.VK_A);
		opcioAltaProfessors.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,ActionEvent.ALT_MASK));
		opcioAltaProfessors.addActionListener(controlador);
		opcioAltaProfessors.setActionCommand("alta_professor");
		
		
		
		opcioAssignacions=new JMenuItem("Assignacions");
		opcioAssignacions.setMnemonic(KeyEvent.VK_B);
		opcioAssignacions.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B,ActionEvent.ALT_MASK));
		opcioAssignacions.addActionListener(controlador);
		opcioAssignacions.setActionCommand("assignacions");
		
		
		menuGestio.add(opcioAltaProfessors);
		menuGestio.add(opcioAssignacions);
		barraMenu.add(menuGestio);
		return barraMenu;
		
	}
	
	private class Controlador implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Object obj = e.getSource();
			if(obj instanceof JMenuItem) {
				JMenuItem jmi = (JMenuItem) obj;
				if(jmi.getActionCommand().equals("alta_professor")) {
					System.out.println("alta professor");	
					Controlador_de_Presentacio.canviFormulari(new FormulariAltaProfessor());
				}else if(jmi.getActionCommand().equals("assignacions")){
					System.out.println("assignacions");
					Controlador_de_Presentacio.canviFormulari(new FormulariAssignacio());
				}
			}
			
		}
		
	}
}

package cat.almata.Examen.presentacio;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.github.lgooddatepicker.components.DatePicker;

import cat.almata.Examen.utils.Fonts;
import cat.almata.Examen.utils.Util;







public class FormulariAltaProfessor extends JInternalFrame implements Formulari {

	private static final long serialVersionUID = 1L;

	private GridBagLayout layout;
	private Controlador controlador;
	private FormulariAltaProfessor fap;

	private JButton btnImatge;
	private JLabel lblImatge;
	
	private String camiAbsolut;
	
	private JLabel lblUsuari;
	private JTextField txtUsuari;
	
	private JLabel lblNom;
	private JTextField txtNom;
	
	private JLabel lblCognoms;
	private JTextField txtCognoms;
	
	private JLabel lblEdat;
	private JTextField txtEdat;
	
	private JLabel lblDatanaixement;
	private DatePicker txtDatanaixement;
	
	private JLabel lblCarnet;
	private JCheckBox ok;


	private JButton btnAlta;
	private JButton btnCancelar;
	private JPanel pnlBotons;





	public FormulariAltaProfessor() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
	}

	@Override
	public void inicialitzacions() {
		setTitle("Formulari entrada dades del professor");
		getContentPane().setLayout(layout=new GridBagLayout());
		Util.treureBarraTitolInteralFrame(this);
		controlador=new Controlador();
		this.setVisible(true);
		fap=this;
	}

	@Override
	public void crearComponents() {
		btnImatge=new JButton("Carregar imatge...");
		btnImatge.addActionListener(controlador);
		
		lblImatge=new JLabel();
		lblImatge.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		
		
		lblUsuari = new JLabel("Usuari:");
		lblUsuari.setFont(Fonts.fontLabel());

		txtUsuari=new JTextField();
		txtUsuari.setFont(Fonts.fontTextField());
		
		
		
		lblNom = new JLabel("Nom:");
		lblNom.setFont(Fonts.fontLabel());
		
		txtNom=new JTextField();
		txtNom.setFont(Fonts.fontTextField());
		
		
		
		lblCognoms = new JLabel("Cognoms:");
		lblCognoms.setFont(Fonts.fontLabel());

		txtCognoms=new JTextField();
		txtCognoms.setFont(Fonts.fontTextField());
		
		
		
		lblEdat = new JLabel("Edat:");
		lblEdat.setFont(Fonts.fontLabel());

		txtEdat=new JTextField();
		txtEdat.setFont(Fonts.fontTextField());
		
		
		
		lblDatanaixement=new JLabel("Data Naixement:");
		lblDatanaixement.setFont(Fonts.fontLabel());
		
		txtDatanaixement=new DatePicker();
		
		
		
		lblCarnet = new JLabel("Carnet B2:");
		lblCarnet.setFont(Fonts.fontLabel());
		
		ok=new JCheckBox();
		
		
		
		btnAlta=new JButton("Alta");
		btnAlta.addActionListener(controlador);
		btnAlta.setActionCommand("alta");
		btnCancelar=new JButton("Cancelar");
		btnCancelar.addActionListener(controlador);
		btnCancelar.setActionCommand("cancelar");

		pnlBotons=new JPanel();
		pnlBotons.setLayout(new GridLayout(1, 2, 5, 0));
		pnlBotons.add(btnAlta);
		pnlBotons.add(btnCancelar);

	}

	@Override
	public void afegirComponents() {
		add(btnImatge);
		add(lblImatge);
		
		add(lblUsuari);
		add(txtUsuari);
		
		add(lblNom);
		add(txtNom);
		
		add(lblCognoms);
		add(txtCognoms);
		
		add(lblEdat);
		add(txtEdat);
		
		add(lblDatanaixement);
		add(txtDatanaixement);
		
		add(lblCarnet);
		add(ok);

		add(pnlBotons);
	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();

		//btnImatge
		gbc.insets= new Insets(5,5,5,5);
		gbc.gridx=0;//és la primera columna
		gbc.gridy=0;//és la primera fila
		gbc.gridheight=1;//quantes cel·les ocupa d'alçada
		gbc.gridwidth=1;//quantes cel·les ocupa d'amplada
		gbc.weightx=0;//factor de creixement
		gbc.weighty=0;//factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.NONE;
		layout.setConstraints(btnImatge, gbc);
		
		//lblImatge
		gbc.gridx=0;//és la primera columna
		gbc.gridy=1;//és la primera fila
		gbc.gridheight=4;//quantes cel·les ocupa d'alçada
		gbc.gridwidth=1;//quantes cel·les ocupa d'amplada
		gbc.weightx=1;//factor de creixement
		gbc.weighty=1;//factor de creixement
		//gbc.anchor= GridBagConstraints.WEST;
		gbc.fill=GridBagConstraints.BOTH;
		layout.setConstraints(lblImatge, gbc);


		
		//lblUsuari
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=5;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblUsuari, gbc);

		//txtUsuari
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=5;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtUsuari, gbc);
		
		
		
		//lblNom
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=6;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblNom, gbc);

		//txtNom
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=6;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtNom, gbc);
		
		
		
		//lblCognoms
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=7;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblCognoms, gbc);

		//txtCognoms
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=7;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtCognoms, gbc);
		
		
		
		//lblEdat
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=8;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblEdat, gbc);

		//txtEdat
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=8;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtEdat, gbc);
		
		
		
		//lblDatanaixement
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=9;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblDatanaixement, gbc);

		//txtDataNaixement
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=9;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtDatanaixement, gbc);
		
		
		
		//lblCarnet
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=10;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblCarnet, gbc);

		//ok
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=10;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(ok, gbc);
		
		
		
		//pnlBotons
		gbc.gridx=1;		//	primera columna
		gbc.gridy=12;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.EAST;
		gbc.fill= GridBagConstraints.NONE;
		layout.setConstraints(pnlBotons, gbc);
	}
	private class Controlador implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if(obj instanceof JButton) {
				if(((JButton) obj).getActionCommand().equals("alta")) {
					boolean a=false;
					
					if(ok.isSelected()) {
						a = true;
					}
					
					Object[] objOpcions3= {"Acceptar","Cancel·lar"};
					int n= JOptionPane.showInternalOptionDialog(fap.getContentPane(), //Finestra pare
					"Segur que vols d'onar de alta el professor?", //Missatge de la finestra
					"Alta professor", //Títol de la finestra
					JOptionPane.NO_OPTION, //Posarem sempre això.
					JOptionPane.QUESTION_MESSAGE, //Tipus icona
					null, //Si no customitzem la icona. Sinò un ImageIcon
					objOpcions3, //Vector de tipus Object[]
					objOpcions3[0]);//Element per defecte del vector Object[]
					//control del retorn del diàleg (0-primer botó, 1-segon botó, etc)
					if(n==0) {
						Controlador_de_Presentacio.addProfessor(camiAbsolut,
								txtUsuari.getText(),
								txtNom.getText(),
								txtCognoms.getText(),
								Integer.valueOf(txtEdat.getText()),
								txtDatanaixement.getText(),
								a);
						
						System.out.println("L'alta s'ha fet correctament");
						
						esborrarFormulari();
					}
					
					
					
					
					
					
					
					
					
					

				}else if(((JButton) obj).getActionCommand().equals("cancelar")) {
					System.out.println("Cancelar");
					Controlador_de_Presentacio.canviFormulari(new FormulariPerDefecte());
				}else {
					JFileChooser chooser = new JFileChooser();
					int returnVal = chooser.showOpenDialog(fap);
					if(returnVal == JFileChooser.APPROVE_OPTION) {
						String cami=null;
						File file=chooser.getSelectedFile();
						cami= file.getAbsolutePath();//també file.getCanonicalPath();
						camiAbsolut=cami; //Almaçeno el cami
						lblImatge.setIcon( new JLabel(new ImageIcon(cami)).getIcon());
						//Exemple amb imatge redimensionada segons el tamany del contenidor
						//imatge.setIcon( new JLabel(Util.redimensionarImatge(cami, 500, 600)).getIcon());
					}
				}
				


			}

		}
		private void esborrarFormulari() {
			
			txtUsuari.setText("");
			txtNom.setText("");
			txtCognoms.setText("");
			txtEdat.setText("");
			txtDatanaixement.setText("");
			ok.setSelected(false);
			btnImatge.grabFocus();
			
		}
	}
}





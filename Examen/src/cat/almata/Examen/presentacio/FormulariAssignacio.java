package cat.almata.Examen.presentacio;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import cat.almata.Examen.domini.Professor;
import cat.almata.Examen.utils.Util;





public class FormulariAssignacio extends JInternalFrame implements Formulari {
	
	private static final long serialVersionUID = 1L;
	
	private GridBagLayout layout;
	
	
	private JLabel lblProfessors;
	private JLabel lblAssignats;
	
	private JList<String> llista = new JList<String>();
	private DefaultListModel<String> listModel;
	private JScrollPane scroll;
	
	private JList<String> llista2 = new JList<String>();
	private DefaultListModel<String> listModel2;
	private JScrollPane scroll2;
	
	//private JButton btnSortir;
	private JButton btnDreta;
	private JButton btnEsquerra;
	private JPanel panellBotons;
	private Controlador controlador;
	
	
	

	
	
	
	public FormulariAssignacio() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
	}

	
	@Override
	public void inicialitzacions() {
		setTitle("Formulari assignacio de professors");
		getContentPane().setLayout(layout=new GridBagLayout());
		Util.treureBarraTitolInteralFrame(this);
		controlador=new Controlador();
		this.setVisible(true);
	}

	@Override
	public void crearComponents() {

		
		
		lblProfessors=new JLabel("Professors:");
		lblAssignats=new JLabel("Assignats:");
		
		
		btnDreta=new JButton(">>");
		btnDreta.addActionListener(controlador);
		btnDreta.setActionCommand("dreta");
		btnEsquerra=new JButton("<<");
		btnEsquerra.addActionListener(controlador);
		btnEsquerra.setActionCommand("esquerra");
		panellBotons=new JPanel(new GridLayout(2,1));
		panellBotons.add(btnDreta);
		panellBotons.add(btnEsquerra);
		
		
		
		listModel = new DefaultListModel<String>();
		Map<String, Professor> professors = Controlador_de_Presentacio.getAllUsers();
		for(Entry<String, Professor> objecteMapa: professors.entrySet()) {
			Professor profe = objecteMapa.getValue();
			listModel.addElement(profe.getUsuari());
		}
		llista=new JList<String>(listModel);
		llista.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		scroll = new JScrollPane(llista);
		
		listModel2 = new DefaultListModel<String>();
		llista2=new JList<String>(listModel2);
		llista2.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		scroll2 = new JScrollPane(llista2);
		

	}

	@Override
	public void afegirComponents() {

		getContentPane().add(panellBotons);
		add(lblProfessors);
		add(lblAssignats);
		add(scroll);
		
	
		add(scroll2);

	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();
		
		//lblProfes
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblProfessors, gbc);
		
		
		
		//lblAssignats
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=2;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.EAST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblAssignats, gbc);
		
		
		//panellBotons
		gbc.gridx=1;		//	primera columna
		gbc.gridy=2;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.EAST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(panellBotons, gbc);
		
		
		//llista1
		gbc.gridx=0;		//	primera columna
		gbc.gridy=1;		//	primera fila
		gbc.gridheight=3;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=1;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.EAST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(scroll, gbc);
		
		
		
		//llista1
		gbc.gridx=2;		//	primera columna
		gbc.gridy=1;		//	primera fila
		gbc.gridheight=3;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=1;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.EAST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(scroll2, gbc);


	}
	private class Controlador implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if(obj instanceof JButton) {
				JButton boto = (JButton) obj;
				if(boto.getActionCommand().equals("dreta")) {
					
					
					ArrayList<String> seleccionats = new ArrayList<String>(llista.getSelectedValuesList());
					for(String element: seleccionats) {
						listModel2.addElement(element);
						listModel.removeElement(element);
					}
						
					
					
				}else if(boto.getActionCommand().equals("esquerra")) {
					ArrayList<String> seleccionats = new ArrayList<String>(llista2.getSelectedValuesList());
					for(String element: seleccionats) {
						listModel.addElement(element);
						listModel2.removeElement(element);
						System.out.println("hola");
					}
				}
				
			}
			
		}

	}



}

package cat.almata.Examen.bbdd;

import java.util.Map;
import java.util.TreeMap;

import cat.almata.Examen.domini.Professor;



public class HelperBBDD {
	private static Map<String, Professor> taulaProfessors = new TreeMap<String, Professor>(); 
	
	public static void addProfessor(Professor profe) {
		taulaProfessors.put(profe.getUsuari(), profe);
	}
	public static Map<String, Professor> getAllProfessors(){
		return taulaProfessors;
	}
}

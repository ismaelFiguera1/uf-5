package cat.almata.a010;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

public class Ppal extends JFrame{
	private JList<String> llista = new JList<String>();
	private JButton btnAfegir;
	private JButton btnEsborrar;
	private JPanel pnlBotons;
	private JTextField txtElement;
	private DefaultListModel<String> listModel;
	private JScrollPane scroll;

	public Ppal() {
		setTitle("JList");
		setSize(400,500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		listModel = new DefaultListModel<String>();
		llista=new JList<String>(listModel);
		llista.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		scroll = new JScrollPane(llista);
		JPanel panell = new JPanel(new BorderLayout());



		txtElement = new JTextField();
		txtElement.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if(txtElement.getText().length()==8) {
					e.consume();
				}
			}
		});
		
		btnAfegir = new JButton("Afegir");
		btnEsborrar=new JButton("Esborrar");


		btnAfegir.addActionListener(
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						String text = txtElement.getText().trim();
						if(!text.isEmpty()) {
							listModel.addElement(text);
							txtElement.setText("");
						}
						txtElement.grabFocus();
					}
				}
				);

		btnEsborrar.addActionListener(
				new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						ArrayList<String> seleccionats = new ArrayList<String>(llista.getSelectedValuesList());
						for(String element: seleccionats) {
							listModel.removeElement(element);
						}
					}
				}
				);

		pnlBotons = new JPanel(new GridLayout(2,1));
		pnlBotons.add(btnAfegir);
		pnlBotons.add(btnEsborrar);

		panell.add(txtElement, BorderLayout.NORTH);
		panell.add(scroll, BorderLayout.CENTER);
		panell.add(pnlBotons, BorderLayout.SOUTH);

		add(panell);
	}
	public static void main(String[] args) {


		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				Ppal a = new Ppal();
				a.setVisible(true);

			}
		});
	}
}

package cat.almata.a003;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Ppal {

	public static void main(String[] args) {
		ArrayList<String> frase = new ArrayList<String>();
		frase.add("Hola");
		frase.add("que");
		frase.add("tal.");
		
		for(String paraula : frase) {
			System.out.print(paraula+" ");
		}

		System.out.println("\n"+frase.get(0));
		System.out.println(frase.get(2));
		
		List<Persona> persones = new LinkedList<Persona>();
		persones.add(new Persona("Alex", "a001"));
		persones.add(new Persona("Raul", "a002"));
		persones.add(new Persona("Jordi", "a003"));
		System.out.println(persones);	//	Aixo imprimeix la coleccio amb el metode toString de Persona
		
		System.out.println();
		
		for(Persona a : persones) {
			System.out.println(a);
		}
		
		System.out.println();
		
		if(persones.contains(new Persona("a002"))) {
			System.out.println("La persona SI existeix.");
		}else {
			System.out.println("La persona NO existeix.");
		}
		
		System.out.println("El Jordi es a la posicio: "+ persones.indexOf(new Persona("a003")));
		
		System.out.println();
		
		
		
		frase.clear();
		
		if(persones.isEmpty()) System.out.println("La coleccio de persones es buida");
		else System.out.println("La coleccio de persones no es buida");
		
		if(frase.isEmpty()) System.out.println("La coleccio de frases es buida");
		else System.out.println("La coleccio de frases no es buida");
		
		persones.remove(new Persona("a001"));
		
		System.out.println();
		
		for(Persona a : persones) {
			System.out.println(a);
		}
		
		
		//	Ara fem anar LINKEDLIST
	/*	LinkedList<Persona> persones2 = new LinkedList<Persona>();
		persones2.add(new Persona("Julia", "a001"));
		System.out.println(persones2.get(0));
	*/	
	}

}

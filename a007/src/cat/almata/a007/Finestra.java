package cat.almata.a007;

import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

//	Aquesta sera la meva finestra per a l'aplicacio

public class Finestra extends JFrame implements Formulari{
	
	private static final long serialVersionUID = 1L;
	
	
	//	Components del formulari
	private GridBagLayout layout;
	private JLabel lblUser;
	private JLabel lblPassword;
	private JTextField txtUser;
	private JTextField txtPassword;
	private JButton btnAcceptar;
	

	public Finestra() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
	}

	@Override
	public void inicialitzacions() {
		
		setBounds(500, 200, 400, 200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Formulari entrada usuari i contrasenya.");
		getContentPane().setLayout(layout=new GridBagLayout());
	}

	@Override
	public void crearComponents() {
		lblUser=new JLabel("Usuari");
		txtUser=new JTextField(20);
		lblPassword=new JLabel("Password");
		txtPassword=new JTextField(20);
		btnAcceptar=new JButton("Acceptar.");
	}

	@Override
	public void afegirComponents() {
		getContentPane().add(lblUser);
		getContentPane().add(lblPassword);
		getContentPane().add(txtUser);
		getContentPane().add(txtPassword);
		getContentPane().add(btnAcceptar);
	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();
		//lblUser
		gbc.gridx=0;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblUser, gbc);
		
		
		//txtUser
				gbc.gridx=1;		//	primera columna
				gbc.gridy=0;		//	primera fila
				gbc.gridheight=1;	//	alçada
				gbc.gridwidth=1;	//	llargada
				gbc.weightx=0;		//	factor de creixement
				gbc.weighty=0;		//	factor de creixement
				gbc.anchor= GridBagConstraints.WEST;
				gbc.fill= GridBagConstraints.HORIZONTAL;
				layout.setConstraints(txtUser, gbc);
		
				
				
		//lblPassword
		gbc.gridx=0;		//	primera columna
		gbc.gridy=1;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblPassword, gbc);
		
		
		
		
		
		//txtPassword
		gbc.gridx=1;		//	primera columna
		gbc.gridy=1;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtPassword, gbc);
		
		
		//btnAcceptar
		gbc.gridx=1;		//	primera columna
		gbc.gridy=2;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.EAST;
		gbc.fill= GridBagConstraints.NONE;
		layout.setConstraints(btnAcceptar, gbc);
		
		
		
		
		
		
	}
	
}

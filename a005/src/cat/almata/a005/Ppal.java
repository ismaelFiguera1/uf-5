package cat.almata.a005;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Ppal {

	public static void main(String[] args) {
		Map<String,Persona> mapaPersones = new HashMap<String, Persona>();
		Persona p1 = new Persona("Joan","a1");
		Persona p2 = new Persona("Maria","a2");
		Persona p3 = new Persona("Miquel","a3");
		
		
		
		mapaPersones.put("a1", p1);
		mapaPersones.put("a2", p2);
		mapaPersones.put("a3", p3);
		
		System.out.println(mapaPersones.get("a2").getNom());
		
		
		//	recorrem ficant les claus en una coleccio set i ho recorrem els strings de la coleccio keys
		Set<String> keys=mapaPersones.keySet();
		for(String a:keys) {
			System.out.println(mapaPersones.get(a));
		}
		
		mapaPersones.remove("a1", p1);
		System.out.println();
		
		mapaPersones.put("a06", new Persona("Alex", "a06"));

		
		mapaPersones.remove("a3");		
		for(String i : mapaPersones.keySet()) {
			System.out.println(mapaPersones.get(i));
		}
		
		

	}

}
